from django.shortcuts import render


def render_prediction_params(request):
    return render(request, '../templates/prediction_params/prediction_params.html', locals())

