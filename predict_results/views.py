# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from model.filter import filter_data, normalize_data
from model.form import Form
from model.model import predict
import numpy
import sys


def render_results(request):
    params_dict = dict()
    params_dict = request.POST
    sqf = 0.09290304
    subway_station = params_dict.get("subway_station").encode('utf8')
    time_to_subway = prepare_time(params_dict.get("time_to_subway"))
        #params_dict.get("time_to_subway").replace(' ', '').replace('-', 'min~').encode('utf8')
    time_to_busstop =prepare_time(params_dict.get("time_to_busstop"))
    square = float(params_dict.get("square"))/sqf
    form = Form(subway_station, time_to_subway, time_to_busstop, square)
    filtred_data = filter_data(form)

    trainX, trainY, testX, testY, scaler, dataset, train_size = normalize_data(filtred_data)

    if dataset is not None:
        pred, test, rmse, mape = predict(form, trainX, trainY, testX, testY, scaler, dataset, train_size)
        graph_title = "Прогноз по квартирам площадью от " + str(int((square-100)*sqf))+" кв.м. до " + \
                      str(int((square+100)*sqf)) + " кв.м. близ станции метро " + subway_station
        table_title = "Спрогнозированное и тестовое множества"

        data = prepare_data_for_table(test, pred)

    else:
        graph_title = "Извините, по задданным параметрам нет данных!"
    path_to_img = "../static/images/" + form.get_uri() + ".png"
    return render(request, '../templates/prediction_results/prediction_results.html', locals())


def prepare_time(time_to_sb):
    return time_to_sb.encode('utf8').replace(' ', '').replace('-', 'min~')


def prepare_data_for_table(test_dataset, predict_dataset):
    ready_data = []
    test_dataset = numpy.reshape(test_dataset, (test_dataset.shape[0], 1))
    predict_dataset = numpy.reshape(predict_dataset, (predict_dataset.shape[0], 1))
    for i in range((test_dataset.shape[0]//15+1)*2):
        if i % 2 == 0:
            mass = test_dataset[i/2*15: (i/2+1)*15].astype(int)
        else:
            mass = predict_dataset[i//2 * 15: (i//2+1) * 15].astype(int)
        ready_data.append(mass)
    return ready_data


