
function clickDropDownItem(idItem, idList) {
    var item = $(idItem)
    var list = $(idList)
    list.text(item.text())
}

function getData() {
    var data = {}
    data.subway_station = $('#dropdown-station').text();
    data.time_to_subway = $('#dropdown-time-subway').text();
    data.time_to_busstop = $('#dropdown-time-busstop').text();
    data.square = $('#squre').val();
    var csrf_token =$('#form [name="csrfmiddlewaretoken"]').val();
    data["csrfmiddlewaretoken"] = csrf_token;

    var url = $('#form').attr("action")
    startLoadingAnimation();
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
            stopLoadingAnimation();
            $("div.general-container").html(data)
        },
        error: function () {
            console.log("error")
        }
    });
    console.log(data)
}

function startLoadingAnimation()
{
  var imgObj = $('#loader');
  var card = $('#loader-card');

  imgObj.show();
  card.show();

  var center_card_Y = ($(window).height() - card.height())/2;
  var center_card_X = ($(window).width() - card.width())/2;

  card.offset({top:center_card_Y, left:center_card_X});
  $('#predict-btn').prop("disabled",true);
}

function stopLoadingAnimation()
{
  $('#loader').hide();
  $('#loader-card').hide();
}