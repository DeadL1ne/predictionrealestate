# -*- coding: utf-8 -*-
import numpy
import os
import matplotlib.pylab as plt
import matplotlib as mpl
import numpy as np


def plot_graph(dataset, testPredict, trainX, look_back, scaler, path, real_predict):
    if not os.path.isfile(path):
        testPredictPlot = numpy.empty_like(dataset)
        testPredictPlot[:, :] = numpy.nan
        testPredictPlot[len(trainX) + (look_back):len(dataset) - 1, :] = testPredict
        realPredictPlot = numpy.empty([1000, 1])
        realPredictPlot[:, :] = numpy.nan
        realPredictPlot[len(dataset) - 2: len(dataset)+10, :] = real_predict

        mpl.rcParams['figure.figsize'] = (13.0, 6.0)
        date = generate_date(dataset)
        fig = plt.figure()
        ax1 = fig.add_subplot(1, 1, 1)
        #fig.add_axes(date)
        #ax1 = fig.set_label(date)
        ax1.plot(scaler.inverse_transform(dataset[:len(dataset)-1, :]), label=u"Исходный ряд", linestyle=':')
        ax1.plot(testPredictPlot, label=u"Спрогнозированный ряд", linestyle='-')
        ax1.plot(realPredictPlot, label=u"Реальный прогноз", linestyle='-.')
        ax1.legend()
        #ax1.xaxis.set_major_locator(date)
        plt.xticks(np.arange(0, len(dataset)-1, step=(len(dataset)+12)/(21+0.9)), date, rotation='vertical')
        #ax1.set_xticklabels(date)
        plt.savefig(path)
        plt.clf()


def generate_date(dataset):
    month = 8
    year = 2007
    #step = 120//len(dataset)+1
    step = 6
    print(step)
    date = numpy.empty(21)
    date[:] = numpy.nan
    for i in range(21):
        date[i] = str(month) + '.' + str(year)
        if month+step >= 12:
            month = month+step-12
            year = year+1
        else:
            month = month+step
    print date
    return date





