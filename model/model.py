import numpy
import os
from keras.callbacks import ReduceLROnPlateau
from keras.layers import BatchNormalization, LeakyReLU
from keras.layers.core import Dense, Activation
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.layers import LSTM, Dropout, TimeDistributed, GRU
from keras.callbacks import ModelCheckpoint
from keras.optimizers import RMSprop
from graph import plot_graph
from keras import backend as K
from keras import metrics
from sklearn.metrics import mean_squared_error, mean_absolute_error

import tensorflow as tf


numpy.random.seed(7)
look_back = 1


def LSTM_model():
    model = Sequential()
    # model.add(LSTM(64, input_shape=(look_back, 1), activation='relu', return_sequences=True))
    # model.add(Dropout(0.5))
    # model.add(Dense(1, activation='relu'))
    model.add(LSTM(32, input_shape=(look_back, 1), activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='relu'))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def GRU_model():
    model = Sequential()
    # model.add(GRU(32, input_shape=(look_back, 1), activation='relu', return_sequences=True))
    # model.add(Dropout(0.1))
    # model.add(Dense(1, activation='relu'))
    model.add(GRU(32, input_shape=(look_back, 1), activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='relu'))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def build_model(form, trainX, trainY):
    path = "static/models/" + form.get_uri() + ".hdf5"
    K.clear_session()
    if os.path.isfile(path):
        net_model = load_model(path)
        return net_model
    else:
        net_model = GRU_model()
        net_model.fit(trainX, trainY, callbacks=[ModelCheckpoint(path,
                                                                 monitor='val_loss', save_best_only=True,
                                                                 save_weights_only=False, mode="min")], epochs=100,
                      batch_size=1, validation_data=(trainX, trainY), verbose=2)
        net_model = load_model(path)
        return net_model


def predict(form, trainX, trainY, testX, testY, scaler, dataset, train_size):
    model = build_model(form, trainX, trainY)
    testX = numpy.reshape(testX, (testX.shape[0], 1, 1))
    testPredict = model.predict(testX)
    last_prediction = numpy.empty_like(testX)
    last_prediction[:, :] = numpy.nan
    last_prediction = testX[len(testX) - 1::]
    real_predict = numpy.empty([12, 1])
    real_predict[:, :] = numpy.nan
    for i in range(12):
        last_prediction = numpy.reshape(last_prediction, (len(last_prediction), 1, 1))
        last_prediction = model.predict(last_prediction)
        real_predict[i] = last_prediction
    #real_predict = model.predict(numpy.reshape(real_predict, (real_predict.shape[0], 1, 1)))
    real_predict = scaler.inverse_transform(real_predict)
    testPredict = scaler.inverse_transform(testPredict)
    path = "static/images/" + form.get_uri() + ".png"
    plot_graph(dataset, testPredict, trainX, look_back, scaler, path, real_predict)
    #testY = scaler.inverse_transform([testY])
    #testY = numpy.reshape(testY, (testY.shape[1], 1))
    testY = dataset[int(train_size):len(dataset)-1]
    testY = scaler.inverse_transform(testY)
    rmse, mape = calculate_errors(testPredict, testY)
    return testPredict, testY, rmse, mape


def calculate_errors(predict_values, test_values):
    rmse = round(numpy.math.sqrt(mean_squared_error(test_values[:, 0], predict_values[:, 0])), 3)
    mape = round(mean_absolute_percentage_error(test_values[0], predict_values[0]), 3)
    return rmse, mape


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = numpy.array(y_true), numpy.array(y_pred)
    return numpy.mean(numpy.abs((y_true - y_pred) / y_true)) * 100

