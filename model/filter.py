import pandas as pan
import numpy
from sklearn.preprocessing import MinMaxScaler


def filter_data(form):
    df = pan.read_csv('static/Daegu_Real_Estate_data.csv', sep=',', usecols=["SalePrice", "YrSold", "MonthSold", "Size(sqf)",
                                                                      "TimeToBusStop", "TimeToSubway", "SubwayStation"],)
    df = df.loc[(df['SubwayStation'] == form.subway_station) & (df['TimeToBusStop'] == form.time_to_busstop) & (df['TimeToSubway'] == form.time_to_subway) &
           (df['Size(sqf)'] > form.square - 100) & (df['Size(sqf)'] < form.square + 100)]
    return df


def create_dataset2(dataset):
    dataX, dataY = [], []
    for i in range(len(dataset)-1):
        a = dataset[i, 0]
        dataX.append(a)
        dataY.append(dataset[i+1, 0])
    return numpy.array(dataX), numpy.array(dataY)


def normalize_data(dataframe):
    if dataframe["SalePrice"].count() < 20:
        return None, None, None, None, None, None, None
    dataset = dataframe["SalePrice"].values


    # fix_data(dataframe)

    dataset = dataset.astype('float32')
    # normalize data
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataset = numpy.reshape(dataset, (dataset.shape[0], 1))
    dataset = scaler.fit_transform(dataset)
    train_size = round(dataframe["SalePrice"].count() * 0.9)
    train, test = dataset[:int(train_size):], dataset[int(train_size):len(dataset):]

    trainX, trainY = create_dataset2(train)
    testX, testY = create_dataset2(test)

    trainX = numpy.reshape(trainX, (trainX.shape[0], 1, 1))
    testX = numpy.reshape(testX, (testX.shape[0], 1, 1))
    return trainX, trainY, testX, testY, scaler, dataset, train_size
