class Form(object):

    def __init__(self, subway_stantion, time_to_subway, time_to_busstop, square):
        self.subway_station = subway_stantion
        self.time_to_subway = time_to_subway
        self.time_to_busstop = time_to_busstop
        self.square = square

    def get_uri(self):
        return str(self.subway_station)+str(self.time_to_subway)+str(self.time_to_busstop)+\
               str(int(self.square*0.09290304))
